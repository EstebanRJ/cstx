package com.banamex.citiscreening.repository;


import com.banamex.citiscreening.model.TP_5_DVL_EXT;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CitiCTSXTp_5_dvl_extRepository extends CrudRepository <TP_5_DVL_EXT, String> {
    TP_5_DVL_EXT findByTransactionId(String transactionId);
}
