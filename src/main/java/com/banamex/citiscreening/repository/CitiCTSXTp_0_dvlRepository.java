package com.banamex.citiscreening.repository;

import com.banamex.citiscreening.model.TP_0_DVL;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CitiCTSXTp_0_dvlRepository extends CrudRepository<TP_0_DVL, String> {
        TP_0_DVL findByTransactionId(String transactionId);
    }

