package com.banamex.citiscreening.repository;

import com.banamex.citiscreening.model.VM_DASHBOARD;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CitiCSTXVm_dashRepository extends CrudRepository<VM_DASHBOARD, Integer> {

	List<VM_DASHBOARD> findBySourceSystem(String srcSystem);
}
