package com.banamex.citiscreening.repository;


import com.banamex.citiscreening.model.TP_4_PRTC_PRTC;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CitiCTSXTp_4_prtc_prtcRepository extends CrudRepository<TP_4_PRTC_PRTC, String> {
    TP_4_PRTC_PRTC findByTransactionId(String transactionId);
}
