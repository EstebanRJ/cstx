package com.banamex.citiscreening.repository;


import com.banamex.citiscreening.model.TR_CSTX_RESPONSE;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CitiCSTXTr_cstx_responseRepository extends CrudRepository <TR_CSTX_RESPONSE, String> {
    TR_CSTX_RESPONSE findByTransactionId(String transactionId);
}

