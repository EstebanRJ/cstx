package com.banamex.citiscreening.repository;

import com.banamex.citiscreening.model.VM_GOS;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CitiCSTXVm_gosRepository extends CrudRepository<VM_GOS, String> {

}
