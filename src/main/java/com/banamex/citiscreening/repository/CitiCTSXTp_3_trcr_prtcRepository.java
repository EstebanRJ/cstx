package com.banamex.citiscreening.repository;


import com.banamex.citiscreening.model.TP_3_TRCR_PRTC;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CitiCTSXTp_3_trcr_prtcRepository extends CrudRepository<TP_3_TRCR_PRTC, String> {
    TP_3_TRCR_PRTC findByTransactionId(String transactionId);
}
