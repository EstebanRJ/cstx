package com.banamex.citiscreening.repository;


import com.banamex.citiscreening.model.TP_2_PRTC_TRCR;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface CitiCTSXTp_2_prtc_trcrRepository extends  CrudRepository<TP_2_PRTC_TRCR, String> {
        TP_2_PRTC_TRCR findByTransactionId(String transactionId);


    }
