package com.banamex.citiscreening.repository;

import com.banamex.citiscreening.model.TerceroTercero;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CitiCTSXTerTerRepository extends CrudRepository<TerceroTercero, String> {

	List<TerceroTercero> findByTransactionId(String tracking);

}
