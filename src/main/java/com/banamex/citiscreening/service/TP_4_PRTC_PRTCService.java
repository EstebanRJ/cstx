package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.TP_4_PRTC_PRTC;

import java.util.List;

public interface TP_4_PRTC_PRTCService {
    List<TP_4_PRTC_PRTC> findAll();
    TP_4_PRTC_PRTC findByTransactionId(String tracking);
}
