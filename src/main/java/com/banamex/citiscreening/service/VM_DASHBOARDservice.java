package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.VM_DASHBOARD;

import java.util.List;


public interface VM_DASHBOARDservice {

    List<VM_DASHBOARD> findAll();
    List<VM_DASHBOARD> findBySourceSystem(String srcSystem);
}
