package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.TerceroTercero;
import java.util.List;

public interface TerceroTerceroService {

    List<TerceroTercero> findAll();
    TerceroTercero findByTransactionId(String tracking);
}
