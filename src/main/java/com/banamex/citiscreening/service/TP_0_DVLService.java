package com.banamex.citiscreening.service;

import com.banamex.citiscreening.model.TP_0_DVL;

import java.util.List;

public interface TP_0_DVLService {
	
    List<TP_0_DVL> findAll();
    TP_0_DVL findByTransactionId(String tracking);
    
}
