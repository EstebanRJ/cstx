package com.banamex.citiscreening.service;

import com.banamex.citiscreening.model.VM_GOS;
import java.util.List;

public interface VM_GOSService {

	List<VM_GOS> findAll();
}
