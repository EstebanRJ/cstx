package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.TP_5_DVL_EXT;
import com.banamex.citiscreening.repository.CitiCTSXTp_5_dvl_extRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("TP_5_DVL_EXTService")
public class TP_5_DVL_EXTServicelmpl implements TP_5_DVL_EXTService{

    @Autowired
    private CitiCTSXTp_5_dvl_extRepository tp_5_dvl_extRepository;

    public List<TP_5_DVL_EXT> findAll(){ 
    	
    	return (List<TP_5_DVL_EXT>) tp_5_dvl_extRepository.findAll();
    }

    @Override
    public TP_5_DVL_EXT findByTransactionId(String tracking){
    	
        return tp_5_dvl_extRepository.findOne(tracking);
    }

}
