package com.banamex.citiscreening.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banamex.citiscreening.model.VM_GOS;
import com.banamex.citiscreening.repository.CitiCSTXVm_gosRepository;

import java.util.ArrayList;
import java.util.List;

@Service("VM_GOSService")
public class VM_GOSServiceImpl implements VM_GOSService {

	   @Autowired
	   private CitiCSTXVm_gosRepository vmGosRepository;

	   public List<VM_GOS> findAll(){
	        
		   /*
	        Integer incomingTran = 0;
	        Integer ammount1 = 0; 
	        Integer outGoingtTran = 0;
	        Integer ammount2 = 0;
	    	List<VM_GOS> lista = new ArrayList<>();
	    	for(VM_GOS vm : lista) {
	    		incomingTran += vm.getTransIn();
	    		ammount1 += vm.getTransactionAmountIn();
	    		outGoingtTran += vm.getTransOut();
	    		ammount2 += vm.getTransactionAmountOut();
	    	}
	    	*/
	    	return   (List<VM_GOS>)   vmGosRepository.findAll();
	  }

}