package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.TP_2_PRTC_TRCR;
import java.util.List;

public interface TP_2_PRTC_TRCRService {

    List<TP_2_PRTC_TRCR> findAll();

    TP_2_PRTC_TRCR findByTransactionId(String tracking);
}
