package com.banamex.citiscreening.service;

import com.banamex.citiscreening.model.TP_4_PRTC_PRTC;
import com.banamex.citiscreening.repository.CitiCTSXTp_4_prtc_prtcRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("TP_4_PRTC_PRTCService")
public class TP_4_PRTC_PRTCServicelmpl implements TP_4_PRTC_PRTCService {

    @Autowired
    private CitiCTSXTp_4_prtc_prtcRepository tp_4_prtc_prtcRepository;

    public List<TP_4_PRTC_PRTC> findAll(){
        return (List<TP_4_PRTC_PRTC>) tp_4_prtc_prtcRepository.findAll();
    }

    @Override
    public TP_4_PRTC_PRTC findByTransactionId(String tracking){
    	
        return tp_4_prtc_prtcRepository.findOne(tracking);
    }











}
