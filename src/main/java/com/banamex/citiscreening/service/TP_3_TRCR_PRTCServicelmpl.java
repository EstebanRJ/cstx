package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.TP_3_TRCR_PRTC;
import com.banamex.citiscreening.repository.CitiCTSXTp_3_trcr_prtcRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("TP_3_TRCR_PRTCService")
public class TP_3_TRCR_PRTCServicelmpl implements TP_3_TRCR_PRTCService {

    @Autowired
    private CitiCTSXTp_3_trcr_prtcRepository tp_3_Trcr_PrtcRepository;

    public List<TP_3_TRCR_PRTC> findAll() {

        return (List<TP_3_TRCR_PRTC>) tp_3_Trcr_PrtcRepository.findAll();
    }

    @Override
    public TP_3_TRCR_PRTC findByTransactionId(String tracking){
    	
        return tp_3_Trcr_PrtcRepository.findOne(tracking);
    }










}
