package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.TerceroTercero;
import com.banamex.citiscreening.repository.CitiCTSXTerTerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service("TerceroTerceroService")
public class TerceroTerceroServiceImpl implements TerceroTerceroService {


    @Autowired
    private CitiCTSXTerTerRepository terceroTerceroRepository;

    public List<TerceroTercero> findAll(){

        return   (List<TerceroTercero>)   terceroTerceroRepository.findAll();
    }
    
    @Override
    public TerceroTercero findByTransactionId(String tracking) {
    	
    	//return (List<TerceroTercero>) terceroTerceroRepository.findByTransactionId(tracking);
    	return terceroTerceroRepository.findOne(tracking);
    }

    
}

