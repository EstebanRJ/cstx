package com.banamex.citiscreening.service;

import com.banamex.citiscreening.model.TR_CSTX_RESPONSE;

import java.util.List;

public interface TR_CSTX_RESPONSEService {
    List<TR_CSTX_RESPONSE> findAll();
    TR_CSTX_RESPONSE findByTransactionId(String tracking);

}
