package com.banamex.citiscreening.service;

import com.banamex.citiscreening.model.TP_5_DVL_EXT;

import java.util.List;

public interface TP_5_DVL_EXTService {
    List<TP_5_DVL_EXT> findAll();
    TP_5_DVL_EXT findByTransactionId(String tracking);

}
