package com.banamex.citiscreening.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banamex.citiscreening.model.TP_0_DVL;
import com.banamex.citiscreening.repository.CitiCTSXTp_0_dvlRepository;

import java.util.List;

@Service("TP_0_DVLService")
public class TP_0_DVLServicelmpl implements TP_0_DVLService{

    @Autowired
    private CitiCTSXTp_0_dvlRepository tp_0_dvlRepository;


    
    public List<TP_0_DVL> findAll(){
    	
        return   (List<TP_0_DVL>)   tp_0_dvlRepository.findAll();
    }

    @Override
    public TP_0_DVL findByTransactionId(String tracking) {
    	
    	return tp_0_dvlRepository.findOne(tracking);
    }
}

