package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.TP_2_PRTC_TRCR;
import com.banamex.citiscreening.repository.CitiCTSXTp_2_prtc_trcrRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service("TP_2_PRTC_TRCRService")
public class TP_2_PRTC_TRCRServicelmpl implements TP_2_PRTC_TRCRService {

    @Autowired
    private CitiCTSXTp_2_prtc_trcrRepository tp_2_Prtc_TrcrRepository;


    public List<TP_2_PRTC_TRCR> findAll() {

        return (List<TP_2_PRTC_TRCR>)  tp_2_Prtc_TrcrRepository.findAll();
    }

    @Override
    public TP_2_PRTC_TRCR findByTransactionId(String tracking) {
    	
    	return tp_2_Prtc_TrcrRepository.findOne(tracking);
    }


}