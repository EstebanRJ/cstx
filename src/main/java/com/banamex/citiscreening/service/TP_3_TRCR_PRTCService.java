package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.TP_3_TRCR_PRTC;

import java.util.List;

public interface TP_3_TRCR_PRTCService {
    List<TP_3_TRCR_PRTC> findAll();

    TP_3_TRCR_PRTC findByTransactionId(String tracking);
}
