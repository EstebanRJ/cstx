package com.banamex.citiscreening.service;


import com.banamex.citiscreening.model.TR_CSTX_RESPONSE;
import com.banamex.citiscreening.repository.CitiCSTXTr_cstx_responseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("TR_CSTX_RESPONSEService")
public class TR_CSTX_RESPONSEServiceImpl implements TR_CSTX_RESPONSEService {

    @Autowired
    private CitiCSTXTr_cstx_responseRepository tr_cstx_responseRepository;
    
    public List<TR_CSTX_RESPONSE> findAll(){ 
    	
    	return (List<TR_CSTX_RESPONSE>) tr_cstx_responseRepository.findAll();
    }


    @Override
    public TR_CSTX_RESPONSE findByTransactionId(String tracking){
    	
        return tr_cstx_responseRepository.findOne(tracking);
    }


}
