package com.banamex.citiscreening.service;

import com.banamex.citiscreening.model.VM_DASHBOARD;
import com.banamex.citiscreening.repository.CitiCSTXVm_dashRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service("VM_DASHBOARDservice")
public class VM_DASHBOARDserviceImpl implements VM_DASHBOARDservice {

    @Autowired
    private CitiCSTXVm_dashRepository vm_dashRepository;

    public List<VM_DASHBOARD> findAll(){
        return (List<VM_DASHBOARD>) vm_dashRepository.findAll();
    }
    
    @Override
    public List<VM_DASHBOARD> findBySourceSystem(String srcSystem) {
    	
    	return (List<VM_DASHBOARD>) vm_dashRepository.findBySourceSystem(srcSystem);
    }

}
