package com.banamex.citiscreening;


import com.banamex.citiscreening.utils.DbConnectionProps;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;

import javax.sql.DataSource;

@SpringBootApplication
public class CitiscreeningApplication extends SpringBootServletInitializer {

	@Autowired
	DbConnectionProps props;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(CitiscreeningApplication.class);
	}

	@Bean
	public DataSource dataSource()
	{
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		DataSource dataSource = dataSourceLookup.getDataSource(props.getJndiName());
		return dataSource;
	}


}