package com.banamex.citiscreening.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SaNsErrorCat {

    @RequestMapping(value={"/SaNSErrorCodeCatalog"}, method = RequestMethod.GET)
    public ModelAndView SaNSErrorCodeCatalog(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("SaNSErrorCodeCatalog");
        return modelAndView;
    }

}
