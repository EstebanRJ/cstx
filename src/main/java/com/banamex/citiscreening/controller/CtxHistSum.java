package com.banamex.citiscreening.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class CtxHistSum {

    @RequestMapping(value={"/CitiScreeningHistoricalSummary"}, method = RequestMethod.GET)
    public ModelAndView CitiScreeningHistoricalSummary(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("CitiScreeningHistoricalSummary");
        return modelAndView;
    }

}
