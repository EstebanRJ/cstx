package com.banamex.citiscreening.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.banamex.citiscreening.service.VM_GOSServiceImpl;

@Controller
public class DashGen {
	@Autowired
	private VM_GOSServiceImpl vmGosService;

    @RequestMapping(value={"/DashboardGeneral"}, method = RequestMethod.GET)
    public ModelAndView DashboardGeneral(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("dashGen", vmGosService.findAll());
       /* modelAndView.addObject("totalizadores", incomingTran); */
        modelAndView.setViewName("DashboardGeneral");
        return modelAndView;
    }

}
