package com.banamex.citiscreening.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.banamex.citiscreening.service.TP_0_DVLService;
import com.banamex.citiscreening.service.TP_2_PRTC_TRCRService;
import com.banamex.citiscreening.service.TP_3_TRCR_PRTCService;
import com.banamex.citiscreening.service.TP_4_PRTC_PRTCService;
import com.banamex.citiscreening.service.TP_5_DVL_EXTService;
import com.banamex.citiscreening.service.TerceroTerceroService;



@Controller
public class TransaXSumController {
	@Autowired
	private TerceroTerceroService terceroTerceroService;
	private TP_2_PRTC_TRCRService participantToThirdService;
	private TP_3_TRCR_PRTCService thirdToParticipantService;
	private TP_4_PRTC_PRTCService participantToParticipantService;
	private TP_5_DVL_EXTService   delayedReturnService;
	private TP_0_DVLService       returnService;
	
	@RequestMapping(value={"/TransaXSumController"}, method = RequestMethod.GET)
	public ModelAndView TransactionSummary(@RequestParam("tracking") String tracking, @RequestParam("paymentType") String paymentType){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("tp", paymentType);
		/*
		if(paymentType == "Third to Third") {
			modelAndView.addObject("detail123", terceroTerceroService.findByTransactionId(tracking));
		}
		else if(paymentType == "Participant Third") {
			modelAndView.addObject("detail123", participantToThirdService.findByTransactionId(tracking));
		}
		else if(paymentType == "Third to Participant") {
			modelAndView.addObject("detail123", thirdToParticipantService.findByTransactionId(tracking));
		}
		else if(paymentType == "Participant to Participant") {
			modelAndView.addObject("detail4", participantToParticipantService.findByTransactionId(tracking));
		}
		else if(paymentType == "Delayed Returned") {
			modelAndView.addObject("detail5", delayedReturnService.findByTransactionId(tracking));
		}
		else if(paymentType == "Returned") {
			modelAndView.addObject("detail0", returnService.findByTransactionId(tracking));
		}
		*/
		modelAndView.addObject("detail123", terceroTerceroService.findByTransactionId(tracking));
		modelAndView.setViewName("TransactionSummary");
		return modelAndView;
	}
	
}
