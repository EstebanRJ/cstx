package com.banamex.citiscreening.controller;

/**
 * @author Esteban
 *
 */

import com.banamex.citiscreening.service.VM_DASHBOARDservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PayDash {
	@Autowired
	private VM_DASHBOARDservice vmDashboardService;

	@RequestMapping(value={"/PaymentStatusDashboard"}, method = RequestMethod.GET)
	public ModelAndView PaymentStatusDashboard(){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.addObject("payments", vmDashboardService.findAll());
		modelAndView.setViewName("PaymentStatusDashboard");
		return modelAndView;
	}
	
	/*
	@RequestMapping(value={"/PaymentStatusDashboard"}, method = RequestMethod.GET)
	public String showPage(Model model, @RequestParam(defaultValue="0")) {
		model.addAttribute("data", vmDashboardService.findAll(new PageRequest(page,4)));
		return "index;
	}
	*/
}
