package com.banamex.citiscreening.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.banamex.citiscreening.service.VM_DASHBOARDservice;

@Controller
public class PayDetail {
	@Autowired
	private VM_DASHBOARDservice vmDashboardService;

    @RequestMapping(value={"/PaymentStatusDetail"}, method = RequestMethod.GET)
    public ModelAndView PaymentStatusDetail(@RequestParam("srcSystem") String srcSystem){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("payments", vmDashboardService.findAll());
        modelAndView.setViewName("PaymentStatusDetail");
        return modelAndView;
    }

}
