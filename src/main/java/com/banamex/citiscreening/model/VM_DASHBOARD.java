package com.banamex.citiscreening.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author Esteban
 *
 */
@Entity
@Table(name = "VM_DASHBOARD", schema ="PG_CTSX_DBO01")

public class VM_DASHBOARD {

	@Id
    @Column(name = "TRACKING_KEY")
    private String trackingKey;
	
    @Column(name = "SOURCE_SYSTEM")
    private String sourceSystem;
	
    @Column(name = "TARGET_SYSTEM")
    private String targetSystem;
    
    @Column(name = "AREA_NAME")
    private String areaName;
    
    @Column(name = "DIRECTION")
    private String direction;
    
    @Column(name = "PAYMENT_TYPE_NAME")
    private String paymentTypeName;
    
    @Column(name = "SENDER")
    private String sender;
    
    @Column(name = "RECEIVER")
    private String receiver;
    
    @Column(name = "NAME_PLAYER")
    private String namePayer;
    
    @Column(name = "ACT_PLAYER")
    private BigInteger actPayer;
    
    @Column(name = "NAME_BENEFICIARY")
    private String nameBeneficiary;
    
    @Column(name = "ACT_BENEFICIARY")
    private BigInteger actBeneficiary;
    
    @Column(name = "PAYMENT_AMOUNT")
    private BigDecimal paymentAmount;
    
    @Column(name = "STATUS")
    private String status;
    
    @Column(name = "DIRECTION_IO")
    private String search;
    
    /* Inician Getters y Setters */

	public String getTrackingKey() {
		return trackingKey;
	}

	public void setTrackingKey(String trackingKey) {
		this.trackingKey = trackingKey;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getTargetSystem() {
		return targetSystem;
	}

	public void setTargetSystem(String targetSystem) {
		this.targetSystem = targetSystem;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getPaymentTypeName() {
		return paymentTypeName;
	}

	public void setPaymentTypeName(String paymentTypeName) {
		this.paymentTypeName = paymentTypeName;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getNamePayer() {
		return namePayer;
	}

	public void setNamePayer(String namePayer) {
		this.namePayer = namePayer;
	}

	public BigInteger getActPayer() {
		return actPayer;
	}

	public void setActPayer(BigInteger actPayer) {
		this.actPayer = actPayer;
	}

	public String getNameBeneficiary() {
		return nameBeneficiary;
	}

	public void setNameBeneficiary(String nameBeneficiary) {
		this.nameBeneficiary = nameBeneficiary;
	}

	public BigInteger getActBeneficiary() {
		return actBeneficiary;
	}

	public void setActBeneficiary(BigInteger actBeneficiary) {
		this.actBeneficiary = actBeneficiary;
	}

	public BigDecimal getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(BigDecimal paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}

	@Override
	public String toString() {
		return "VM_DASHBOARD [trackingKey=" + trackingKey + ", sourceSystem=" + sourceSystem + ", targetSystem="
				+ targetSystem + ", areaName=" + areaName + ", direction=" + direction + ", paymentTypeName="
				+ paymentTypeName + ", sender=" + sender + ", receiver=" + receiver + ", namePayer=" + namePayer
				+ ", actPayer=" + actPayer + ", nameBeneficiary=" + nameBeneficiary + ", actBeneficiary="
				+ actBeneficiary + ", paymentAmount=" + paymentAmount + ", status=" + status + ", search=" + search
				+ "]";
	}
    
}
