package com.banamex.citiscreening.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

/**
 * @author Viktor
 *
 */

@Entity
@Table(name = "TP_1_TRCR_TRCR", schema ="PG_CTSX_DBO01")
public class TerceroTercero {

    @Id
    @Column(name = "TRANSACTION_ID")
    private String transactionId;

    @Column(name = "APPLICATIONNAME")
    private  String applicationName;

    @Column(name = "APPLICATIONALIASNAME")
    private  String applicationAliasName;

    @Column(name = "UNIQUETRANSACTIONREFERENCE")
    private  String uniqueTransactionReference;

    @Column(name = "REGIONORIGINATION")
    private String regionOrigination;

    @Column(name = "COUNTRYORIGINATION")
    private String countryOrigination;

    @Column(name = "COUNTRYINTERMEDIARY")
    private String countryIntermediary;

    @Column(name = "COUNTRYDESTINATION")
    private String countryDestination;

    @Column(name = "TRANSACTIONSTAGE")
    private String transactionStage;

    @Column(name = "BUSINESSUNIT")
    private String businessUnit;

    @Column(name = "UNITID")
    private String unitId;

    @Column(name = "MESSAGETYPE")
    private String messageType;

    @Column(name = "SUBMESSAGETYPE")
    private String subMessageType;

    @Column(name = "DIRECTION")
    private String direction;

    @Column(name = "CUTOFFTIME")
    private String cutOffTime;

    @Column(name = "BRANCHNAMEORIGINATION")
    private String branchNameOrigination;

    @Column(name = "BRANCHNAMEDESTINATION")
    private String branchNameDestination;

    @Column(name = "PRIORITY_H")
    private String priorityH;

    @Column(name = "UNIQUEPRODUCT_ID")
    private String uniqueProductId;

    @Column(name = "RULESET")
    private String ruleSet;

    @Column(name = "BUSINESSUSER")
    private String bussinesUser;

    @Column(name = "SCREENINGMODE")
    private String screeningMode;

    @Column(name = "LOOKBACKPERIOD")
    private String lookBackPeriod;

    @Column(name = "SCREENINGCATEGORY")
    private String screeningCategory;

    @Column(name = "SCREENINGTYPE")
    private String screeningType;

    @Column(name = "SECTOR")
    private String sector;

    @Column(name = "BUSINESSLINE")
    private String bussinesLine;

    @Column(name = "PRODUCT")
    private String product;

    @Column(name = "SUBPRODUCT")
    private String subProduct;

    @Column(name = "VERSION")
    private String version;

    @Column(name = "APPCODE")
    private String appCode;

    @Column(name = "BRANCHTXID")
    private String branchTxId;

    @Column(name = "BUSINESSID")
    private String bussinesId;

    @Column(name = "SCREENEDTIMESTAMP")
    private String screenedTimeStamp;

    @Column(name = "ORGCITILEGVEH")
    private String orgCitiLegVeh;

    @Column(name = "ORGCITILEGVEHTYPE")
    private String orgCitiLegVehType;

    @Column(name = "INTERCITILEGVEH")
    private String interCitiLegVeh;

    @Column(name = "INTERCITILEGVEHTYPE")
    private String interCitiLegVehType;

    @Column(name = "DESTCITILEGVEH")
    private String destCitiLegVeh;

    @Column(name = "DESTCITILEGVEHTYPE")
    private String destCitiLegVehType;

    @Column(name = "SYSTEM_DATE")
    private Date systemDate;

    @Column(name = "SYSTEM_HOUR")
    private Date systemHour;

    @Column(name = "TRANSACTION_DATE")
    private Date transactionDate;

    @Column(name = "INDEX_ISSUING_PARTICIPANT")
    private String indexIssuingParticipant;

    @Column(name = "CODE_ISSUING_PARTICIPANT")
    private BigInteger codeIssuingParticipant;

    @Column(name = "INDEX_RECEIVER_PARTICIPANT")
    private String indexReceiverParticipant;

    @Column(name = "CODE_RECEIVER_PARTICIPANT")
    private BigInteger codeReceiverParticipant;

    @Column(name = "FOLIO_INSTRUCTION")
    private BigInteger folioInstruccion;

    @Column(name = "INDEX_CERTIFICATE")
    private String indexCertificate;

    @Column(name = "PRIORITY_B")
    private BigInteger priorityB;

    @Column(name = "NUMBER_TRANSFER_ORDERS")
    private BigInteger numberTransferOrders;

    @Column(name = "NAME_PAYER")
    private String namePayer;

    @Column(name = "ACT_TYPE_PAYER")
    private BigInteger actTypePayer;

    @Column(name = "ACT_PAYER")
    private BigInteger actPayer;

    @Column(name = "CUR_RFC_PAYER")
    private String curRfcPayer;

    @Column(name = "HOME_ADD_PAYER")
    private String homeAddPayer;

    @Column(name = "POSTAL_CODE_PAYER")
    private String postalCodePayer;

    @Column(name = "DT_CONST_PAYER")
    private String dtConstPayer;

    @Column(name = "NAME_BENEFICIARY")
    private String nameBeneficiary;

    @Column(name = "ACT_TYPE_BENEFICIARY")
    private BigInteger actTypeBEneficiary;

    @Column(name = "ACT_BENEFICIARY")
    private BigInteger actBeneficiary;

    @Column(name = "CUR_RFC_BENEFICIARY")
    private String curRfcbeneficiary;

    @Column(name = "CLASSIFICATION_OF_OPERATION")
    private BigInteger classificationOfOperation;

    @Column(name = "CONCEPT_PMT")
    private String conceptPmt;

    @Column(name = "REF_NO")
    private BigInteger refNo;

    @Column(name = "IP_ADDRESS")
    private String ipAddress;

    @Column(name = "PAYER_DT_DATE")
    private String payerDtDate;

    @Column(name = "PAYER_DT_TIME")
    private String payerDtTime;

    @Column(name = "BENEFICIARY_RECD_DT_DATE")
    private String beneficiaryRecdDtDate;

    @Column(name = "BENEFICIARY_RECD_DT_TIME")
    private String beneficiaryRecdDtTime;

    @Column(name = "CURRENCY")
    private String currency;

    @Column(name = "TRANSACTION_AMOUNT")
    private BigDecimal transactionAmount;

    @Column(name = "RETRY")
    private Integer retry;

    @Column(name = "RESPONSE")
    private String response;

    @Column(name = "SYSTEM_ORG")
    private String systemOrg;

    @Column(name = "SOURCE_SYSTEM")
    private String sourceSytem;



    /* Inician Getters y Setters */

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getApplicationAliasName() {
        return applicationAliasName;
    }

    public void setApplicationAliasName(String applicationAliasName) {
        this.applicationAliasName = applicationAliasName;
    }

    public String getUniqueTransactionReference() {
        return uniqueTransactionReference;
    }

    public void setUniqueTransactionReference(String uniqueTransactionReference) {
        this.uniqueTransactionReference = uniqueTransactionReference;
    }

    public String getRegionOrigination() {
        return regionOrigination;
    }

    public void setRegionOrigination(String regionOrigination) {
        this.regionOrigination = regionOrigination;
    }

    public String getCountryOrigination() {
        return countryOrigination;
    }

    public void setCountryOrigination(String countryOrigination) {
        this.countryOrigination = countryOrigination;
    }

    public String getCountryIntermediary() {
        return countryIntermediary;
    }

    public void setCountryIntermediary(String countryIntermediary) {
        this.countryIntermediary = countryIntermediary;
    }

    public String getCountryDestination() {
        return countryDestination;
    }

    public void setCountryDestination(String countryDestination) {
        this.countryDestination = countryDestination;
    }

    public String getTransactionStage() {
        return transactionStage;
    }

    public void setTransactionStage(String transactionStage) {
        this.transactionStage = transactionStage;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getSubMessageType() {
        return subMessageType;
    }

    public void setSubMessageType(String subMessageType) {
        this.subMessageType = subMessageType;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getCutOffTime() {
        return cutOffTime;
    }

    public void setCutOffTime(String cutOffTime) {
        this.cutOffTime = cutOffTime;
    }

    public String getBranchNameOrigination() {
        return branchNameOrigination;
    }

    public void setBranchNameOrigination(String branchNameOrigination) {
        this.branchNameOrigination = branchNameOrigination;
    }

    public String getBranchNameDestination() {
        return branchNameDestination;
    }

    public void setBranchNameDestination(String branchNameDestination) {
        this.branchNameDestination = branchNameDestination;
    }

    public String getPriorityH() {
        return priorityH;
    }

    public void setPriorityH(String priorityH) {
        this.priorityH = priorityH;
    }

    public String getUniqueProductId() {
        return uniqueProductId;
    }

    public void setUniqueProductId(String uniqueProductId) {
        this.uniqueProductId = uniqueProductId;
    }

    public String getRuleSet() {
        return ruleSet;
    }

    public void setRuleSet(String ruleSet) {
        this.ruleSet = ruleSet;
    }

    public String getBussinesUser() {
        return bussinesUser;
    }

    public void setBussinesUser(String bussinesUser) {
        this.bussinesUser = bussinesUser;
    }

    public String getScreeningMode() {
        return screeningMode;
    }

    public void setScreeningMode(String screeningMode) {
        this.screeningMode = screeningMode;
    }

    public String getLookBackPeriod() {
        return lookBackPeriod;
    }

    public void setLookBackPeriod(String lookBackPeriod) {
        this.lookBackPeriod = lookBackPeriod;
    }

    public String getScreeningCategory() {
        return screeningCategory;
    }

    public void setScreeningCategory(String screeningCategory) {
        this.screeningCategory = screeningCategory;
    }

    public String getScreeningType() {
        return screeningType;
    }

    public void setScreeningType(String screeningType) {
        this.screeningType = screeningType;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public String getBussinesLine() {
        return bussinesLine;
    }

    public void setBussinesLine(String bussinesLine) {
        this.bussinesLine = bussinesLine;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(String subProduct) {
        this.subProduct = subProduct;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getAppCode() {
        return appCode;
    }

    public void setAppCode(String appCode) {
        this.appCode = appCode;
    }

    public String getBranchTxId() {
        return branchTxId;
    }

    public void setBranchTxId(String branchTxId) {
        this.branchTxId = branchTxId;
    }

    public String getBussinesId() {
        return bussinesId;
    }

    public void setBussinesId(String bussinesId) {
        this.bussinesId = bussinesId;
    }

    public String getScreenedTimeStamp() {
        return screenedTimeStamp;
    }

    public void setScreenedTimeStamp(String screenedTimeStamp) {
        this.screenedTimeStamp = screenedTimeStamp;
    }

    public String getOrgCitiLegVeh() {
        return orgCitiLegVeh;
    }

    public void setOrgCitiLegVeh(String orgCitiLegVeh) {
        this.orgCitiLegVeh = orgCitiLegVeh;
    }

    public String getOrgCitiLegVehType() {
        return orgCitiLegVehType;
    }

    public void setOrgCitiLegVehType(String orgCitiLegVehType) {
        this.orgCitiLegVehType = orgCitiLegVehType;
    }

    public String getInterCitiLegVeh() {
        return interCitiLegVeh;
    }

    public void setInterCitiLegVeh(String interCitiLegVeh) {
        this.interCitiLegVeh = interCitiLegVeh;
    }

    public String getInterCitiLegVehType() {
        return interCitiLegVehType;
    }

    public void setInterCitiLegVehType(String interCitiLegVehType) {
        this.interCitiLegVehType = interCitiLegVehType;
    }

    public String getDestCitiLegVeh() {
        return destCitiLegVeh;
    }

    public void setDestCitiLegVeh(String destCitiLegVeh) {
        this.destCitiLegVeh = destCitiLegVeh;
    }

    public String getDestCitiLegVehType() {
        return destCitiLegVehType;
    }

    public void setDestCitiLegVehType(String destCitiLegVehType) {
        this.destCitiLegVehType = destCitiLegVehType;
    }

    public Date getSystemDate() {
        return systemDate;
    }

    public void setSystemDate(Date systemDate) {
        this.systemDate = systemDate;
    }

    public Date getSystemHour() {
        return systemHour;
    }

    public void setSystemHour(Date systemHour) {
        this.systemHour = systemHour;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getIndexIssuingParticipant() {
        return indexIssuingParticipant;
    }

    public void setIndexIssuingParticipant(String indexIssuingParticipant) {
        this.indexIssuingParticipant = indexIssuingParticipant;
    }

    public BigInteger getCodeIssuingParticipant() {
        return codeIssuingParticipant;
    }

    public void setCodeIssuingParticipant(BigInteger codeIssuingParticipant) {
        this.codeIssuingParticipant = codeIssuingParticipant;
    }

    public String getIndexReceiverParticipant() {
        return indexReceiverParticipant;
    }

    public void setIndexReceiverParticipant(String indexReceiverParticipant) {
        this.indexReceiverParticipant = indexReceiverParticipant;
    }

    public BigInteger getCodeReceiverParticipant() {
        return codeReceiverParticipant;
    }

    public void setCodeReceiverParticipant(BigInteger codeReceiverParticipant) {
        this.codeReceiverParticipant = codeReceiverParticipant;
    }

    public BigInteger getFolioInstruccion() {
        return folioInstruccion;
    }

    public void setFolioInstruccion(BigInteger folioInstruccion) {
        this.folioInstruccion = folioInstruccion;
    }

    public String getIndexCertificate() {
        return indexCertificate;
    }

    public void setIndexCertificate(String indexCertificate) {
        this.indexCertificate = indexCertificate;
    }

    public BigInteger getPriorityB() {
        return priorityB;
    }

    public void setPriorityB(BigInteger priorityB) {
        this.priorityB = priorityB;
    }

    public BigInteger getNumberTransferOrders() {
        return numberTransferOrders;
    }

    public void setNumberTransferOrders(BigInteger numberTransferOrders) {
        this.numberTransferOrders = numberTransferOrders;
    }

    public String getNamePayer() {
        return namePayer;
    }

    public void setNamePayer(String namePayer) {
        this.namePayer = namePayer;
    }

    public BigInteger getActTypePayer() {
        return actTypePayer;
    }

    public void setActTypePayer(BigInteger actTypePayer) {
        this.actTypePayer = actTypePayer;
    }

    public BigInteger getActPayer() {
        return actPayer;
    }

    public void setActPayer(BigInteger actPayer) {
        this.actPayer = actPayer;
    }

    public String getCurRfcPayer() {
        return curRfcPayer;
    }

    public void setCurRfcPayer(String curRfcPayer) {
        this.curRfcPayer = curRfcPayer;
    }

    public String getHomeAddPayer() {
        return homeAddPayer;
    }

    public void setHomeAddPayer(String homeAddPayer) {
        this.homeAddPayer = homeAddPayer;
    }

    public String getPostalCodePayer() {
        return postalCodePayer;
    }

    public void setPostalCodePayer(String postalCodePayer) {
        this.postalCodePayer = postalCodePayer;
    }

    public String getDtConstPayer() {
        return dtConstPayer;
    }

    public void setDtConstPayer(String dtConstPayer) {
        this.dtConstPayer = dtConstPayer;
    }

    public String getNameBeneficiary() {
        return nameBeneficiary;
    }

    public void setNameBeneficiary(String nameBeneficiary) {
        this.nameBeneficiary = nameBeneficiary;
    }

    public BigInteger getActTypeBEneficiary() {
        return actTypeBEneficiary;
    }

    public void setActTypeBEneficiary(BigInteger actTypeBEneficiary) {
        this.actTypeBEneficiary = actTypeBEneficiary;
    }

    public BigInteger getActBeneficiary() {
        return actBeneficiary;
    }

    public void setActBeneficiary(BigInteger actBeneficiary) {
        this.actBeneficiary = actBeneficiary;
    }

    public String getCurRfcbeneficiary() {
        return curRfcbeneficiary;
    }

    public void setCurRfcbeneficiary(String curRfcbeneficiary) {
        this.curRfcbeneficiary = curRfcbeneficiary;
    }

    public BigInteger getClassificationOfOperation() {
        return classificationOfOperation;
    }

    public void setClassificationOfOperation(BigInteger classificationOfOperation) {
        this.classificationOfOperation = classificationOfOperation;
    }

    public String getConceptPmt() {
        return conceptPmt;
    }

    public void setConceptPmt(String conceptPmt) {
        this.conceptPmt = conceptPmt;
    }

    public BigInteger getRefNo() {
        return refNo;
    }

    public void setRefNo(BigInteger refNo) {
        this.refNo = refNo;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPayerDtDate() {
        return payerDtDate;
    }

    public void setPayerDtDate(String payerDtDate) {
        this.payerDtDate = payerDtDate;
    }

    public String getPayerDtTime() {
        return payerDtTime;
    }

    public void setPayerDtTime(String payerDtTime) {
        this.payerDtTime = payerDtTime;
    }

    public String getBeneficiaryRecdDtDate() {
        return beneficiaryRecdDtDate;
    }

    public void setBeneficiaryRecdDtDate(String beneficiaryRecdDtDate) {
        this.beneficiaryRecdDtDate = beneficiaryRecdDtDate;
    }

    public String getBeneficiaryRecdDtTime() {
        return beneficiaryRecdDtTime;
    }

    public void setBeneficiaryRecdDtTime(String beneficiaryRecdDtTime) {
        this.beneficiaryRecdDtTime = beneficiaryRecdDtTime;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Integer getRetry() {
        return retry;
    }

    public void setRetry(Integer retry) {
        this.retry = retry;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getSystemOrg() {
        return systemOrg;
    }

    public void setSystemOrg(String systemOrg) {
        this.systemOrg = systemOrg;
    }

    public String getSourceSytem() {
        return sourceSytem;
    }

    public void setSourceSytem(String sourceSytem) {
        this.sourceSytem = sourceSytem;
    }


    @Override
    public String toString() {
        return "TerceroTercero{" +
                "transactionId='" + transactionId + '\'' +
                ", applicationName='" + applicationName + '\'' +
                ", applicationAliasName='" + applicationAliasName + '\'' +
                ", uniqueTransactionReference='" + uniqueTransactionReference + '\'' +
                ", regionOrigination='" + regionOrigination + '\'' +
                ", countryOrigination='" + countryOrigination + '\'' +
                ", countryIntermediary='" + countryIntermediary + '\'' +
                ", countryDestination='" + countryDestination + '\'' +
                ", transactionStage='" + transactionStage + '\'' +
                ", businessUnit='" + businessUnit + '\'' +
                ", unitId='" + unitId + '\'' +
                ", messageType='" + messageType + '\'' +
                ", subMessageType='" + subMessageType + '\'' +
                ", direction='" + direction + '\'' +
                ", cutOffTime='" + cutOffTime + '\'' +
                ", branchNameOrigination='" + branchNameOrigination + '\'' +
                ", branchNameDestination='" + branchNameDestination + '\'' +
                ", priorityH='" + priorityH + '\'' +
                ", uniqueProductId='" + uniqueProductId + '\'' +
                ", ruleSet='" + ruleSet + '\'' +
                ", bussinesUser='" + bussinesUser + '\'' +
                ", screeningMode='" + screeningMode + '\'' +
                ", lookBackPeriod='" + lookBackPeriod + '\'' +
                ", screeningCategory='" + screeningCategory + '\'' +
                ", screeningType='" + screeningType + '\'' +
                ", sector='" + sector + '\'' +
                ", bussinesLine='" + bussinesLine + '\'' +
                ", product='" + product + '\'' +
                ", subProduct='" + subProduct + '\'' +
                ", version='" + version + '\'' +
                ", appCode='" + appCode + '\'' +
                ", branchTxId='" + branchTxId + '\'' +
                ", bussinesId='" + bussinesId + '\'' +
                ", screenedTimeStamp='" + screenedTimeStamp + '\'' +
                ", orgCitiLegVeh='" + orgCitiLegVeh + '\'' +
                ", orgCitiLegVehType='" + orgCitiLegVehType + '\'' +
                ", interCitiLegVeh='" + interCitiLegVeh + '\'' +
                ", interCitiLegVehType='" + interCitiLegVehType + '\'' +
                ", destCitiLegVeh='" + destCitiLegVeh + '\'' +
                ", destCitiLegVehType='" + destCitiLegVehType + '\'' +
                ", systemDate=" + systemDate +
                ", systemHour=" + systemHour +
                ", transactionDate=" + transactionDate +
                ", indexIssuingParticipant='" + indexIssuingParticipant + '\'' +
                ", codeIssuingParticipant=" + codeIssuingParticipant +
                ", indexReceiverParticipant='" + indexReceiverParticipant + '\'' +
                ", codeReceiverParticipant=" + codeReceiverParticipant +
                ", folioInstruccion=" + folioInstruccion +
                ", indexCertificate='" + indexCertificate + '\'' +
                ", priorityB=" + priorityB +
                ", numberTransferOrders=" + numberTransferOrders +
                ", namePayer='" + namePayer + '\'' +
                ", actTypePayer=" + actTypePayer +
                ", actPayer=" + actPayer +
                ", curRfcPayer='" + curRfcPayer + '\'' +
                ", homeAddPayer='" + homeAddPayer + '\'' +
                ", postalCodePayer='" + postalCodePayer + '\'' +
                ", dtConstPayer='" + dtConstPayer + '\'' +
                ", nameBeneficiary='" + nameBeneficiary + '\'' +
                ", actTypeBEneficiary=" + actTypeBEneficiary +
                ", actBeneficiary=" + actBeneficiary +
                ", curRfcbeneficiary='" + curRfcbeneficiary + '\'' +
                ", classificationOfOperation=" + classificationOfOperation +
                ", conceptPmt='" + conceptPmt + '\'' +
                ", refNo=" + refNo +
                ", ipAddress='" + ipAddress + '\'' +
                ", payerDtDate='" + payerDtDate + '\'' +
                ", payerDtTime='" + payerDtTime + '\'' +
                ", beneficiaryRecdDtDate='" + beneficiaryRecdDtDate + '\'' +
                ", beneficiaryRecdDtTime='" + beneficiaryRecdDtTime + '\'' +
                ", currency='" + currency + '\'' +
                ", transactionAmount=" + transactionAmount +
                ", retry=" + retry +
                ", response='" + response + '\'' +
                ", systemOrg='" + systemOrg + '\'' +
                ", sourceSytem='" + sourceSytem + '\'' +
                '}';
    }
}
