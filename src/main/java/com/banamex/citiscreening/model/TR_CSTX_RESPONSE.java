package com.banamex.citiscreening.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "TR_CSTX_RESPONSE", schema = "PG_CTSX_DBO01")
public class TR_CSTX_RESPONSE {


    @Id
    @Column(name = "TRANSACTION_ID")
    private String transactionId;

    @Column(name = "REF_NO")
    private BigDecimal refNo;

    @Column(name = "TRANSACTION_DATE")
    private Date transactionDate;

    @Column(name = "MESSAGE_ID")
    private String messageId;

    @Column(name = "RESPONSE_RECEPTION_TIME")
    private String responseReceptionTime;

    @Column(name = "RESPONSE_RECEPTION_DATE")
    private String responseReceptionDate;

    @Column(name = "STATUS")
    private String status;

    @Column(name = "SISTEMA_ID")
    private String sistemaId;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "SYSTEM_RESPONSE")
    private String systemResponse;

//Empiezan los GETTERS AND SETTERS
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public BigDecimal getRefNo() {
        return refNo;
    }

    public void setRefNo(BigDecimal refNo) {
        this.refNo = refNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getResponseReceptionTime() {
        return responseReceptionTime;
    }

    public void setResponseReceptionTime(String responseReceptionTime) {
        this.responseReceptionTime = responseReceptionTime;
    }

    public String getResponseReceptionDate() {
        return responseReceptionDate;
    }

    public void setResponseReceptionDate(String responseReceptionDate) {
        this.responseReceptionDate = responseReceptionDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSistemaId() {
        return sistemaId;
    }

    public void setSistemaId(String sistemaId) {
        this.sistemaId = sistemaId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSystemResponse() {
        return systemResponse;
    }

    public void setSystemResponse(String systemResponse) {
        this.systemResponse = systemResponse;
    }
    //Terminan los GETTERS AND SETTERS


    @Override
    public String toString() {
        return "TR_CSTX_RESPONSE{" +
                "transactionId='" + transactionId + '\'' +
                ", refNo=" + refNo +
                ", transactionDate=" + transactionDate +
                ", messageId='" + messageId + '\'' +
                ", responseReceptionTime='" + responseReceptionTime + '\'' +
                ", responseReceptionDate='" + responseReceptionDate + '\'' +
                ", status='" + status + '\'' +
                ", sistemaId='" + sistemaId + '\'' +
                ", message='" + message + '\'' +
                ", systemResponse='" + systemResponse + '\'' +
                '}';
    }
}
