package com.banamex.citiscreening.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;

/**
 * @author Esteban
 *
 */
@Entity
@Table(name = "VW_GOS", schema ="PG_CTSX_DBO01")

public class VM_GOS {
	
	
    @Column(name = "SYSTEM_ORG")
    private String systemOrg;
	
    @Column(name = "SOURCE_SYSTEM")
    private String sourceSystem;
    
    @Id
    @Column(name = "CURRENCY")
    private String currency;
    
    @Column(name = "TRANS_IN")
    private Integer transIn;
    
    @Column(name = "TRANSACTION_AMOUNT_IN")
    private Integer transactionAmountIn;
    
    @Column(name = "TRANS_OUT")
    private Integer transOut;
    
	@Column(name = "TRANSACTION_AMOUNT_OUT")
    private Integer transactionAmountOut;
	
    public String getSystemOrg() {
		return systemOrg;
	}

	public void setSystemOrg(String systemOrg) {
		this.systemOrg = systemOrg;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getTransIn() {
		return transIn;
	}

	public void setTransIn(Integer transIn) {
		this.transIn = transIn;
	}

	public Integer getTransactionAmountIn() {
		return transactionAmountIn;
	}

	public void setTransactionAmountIn(Integer transactionAmountIn) {
		this.transactionAmountIn = transactionAmountIn;
	}

	public Integer getTransOut() {
		return transOut;
	}

	public void setTransOut(Integer transOut) {
		this.transOut = transOut;
	}

	public Integer getTransactionAmountOut() {
		return transactionAmountOut;
	}

	public void setTransactionAmountOut(Integer transactionAmountOut) {
		this.transactionAmountOut = transactionAmountOut;
	}

	@Override
	public String toString() {
		return "VM_GOS [systemOrg=" + systemOrg + ", sourceSystem=" + sourceSystem + ", currency=" + currency
				+ ", transIn=" + transIn + ", transactionAmountIn=" + transactionAmountIn + ", transOut=" + transOut
				+ ", transactionAmountOut=" + transactionAmountOut + "]";
	}
    
}
